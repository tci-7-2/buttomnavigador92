package com.morales.appmenubutton

import android.os.Bundle
import androidx.activity.enableEdgeToEdge
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat
import androidx.fragment.app.Fragment
import com.morales.appmenubutton.R
import com.google.android.material.bottomnavigation.BottomNavigationView

class MainActivity : AppCompatActivity() {
    private lateinit var bottomNavigationView: BottomNavigationView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        enableEdgeToEdge()
        setContentView(R.layout.activity_main)
        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main)) { v, insets ->
            val systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars())
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom)
            insets
        }

        bottomNavigationView = findViewById(R.id.btnNavigator)

        if (savedInstanceState == null) {
            cambiarFrame(HomeFragment())
        }

        bottomNavigationView.setOnItemSelectedListener {
                menuItem ->
            when(menuItem.itemId){
                R.id.btnHome -> {
                    cambiarFrame(HomeFragment())
                    true
                }
                R.id.btnLista -> {
                    cambiarFrame(ListaFragment())
                    true
                }

                R.id.btnDB -> {
                    cambiarFrame(DbFragment())
                    true
                }

                R.id.btnAcercade ->{
                    cambiarFrame(AcercaFragment())
                    true
                }

                else -> false
            }
        }
    }

    private fun cambiarFrame(fragment: Fragment){
        supportFragmentManager.beginTransaction().replace(R.id.frmContenedor, fragment ).commit();
    }

}