package com.morales.appmenubutton

import android.content.Context
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.morales.appmenubutton.R
import com.morales.appmenubutton.DataBase.Alumno
import com.google.android.material.imageview.ShapeableImageView

class MiAdaptador(
    private var listaAlumnos: List<Alumno>,
    private val context: Context,
    private val itemClickListener: (Alumno) -> Unit
) : RecyclerView.Adapter<MiAdaptador.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MiAdaptador.ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.alumno_item, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val alumno = listaAlumnos[position]
        holder.bind(alumno)
    }

    override fun getItemCount(): Int {
        return listaAlumnos.size
    }

    fun actualizarLista(nuevaLista: List<Alumno>){
        listaAlumnos = nuevaLista
        notifyDataSetChanged()
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val txtNombre : TextView = itemView.findViewById(R.id.txtAlumnoNombre)
        val txtMatricula : TextView = itemView.findViewById(R.id.txtMatricula)
        val txtCarrera : TextView = itemView.findViewById(R.id.txtCarrera)
        val foto : ImageView = itemView.findViewById(R.id.foto)
        fun bind(alumno: Alumno) {
            txtMatricula.text = alumno.matricula
            txtNombre.text = alumno.nombre
            txtCarrera.text = alumno.especialidad

            // Cargar la imagen usando Glide si la URI no es "Pendiente"
            if (alumno.foto != "Pendiente") {
                Glide.with(itemView.context)
                    .load(Uri.parse(alumno.foto))
                    .placeholder(R.mipmap.default_picture) // Mientras carga la imagen, se coloca la predeterminada
                    .error(R.mipmap.default_picture) // Establecer la imagen predeterminada en caso de error
                    .into(foto)
            } else {
                foto.setImageResource(R.mipmap.default_picture)  // Imagen predeterminada
            }

            // Configurar clic en el elemento de la lista
            itemView.setOnClickListener {
                itemClickListener.invoke(alumno)
            }
        }
    }
}