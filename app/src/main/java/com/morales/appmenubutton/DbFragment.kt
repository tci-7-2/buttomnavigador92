package com.morales.appmenubutton

import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.bumptech.glide.Glide
import com.morales.appmenubutton.DataBase.Alumno
import com.morales.appmenubutton.DataBase.AlumnosDbHelper
import com.morales.appmenubutton.DataBase.dbAlumnos
import android.Manifest
import android.provider.MediaStore
import com.morales.appmenubutton.R

private const val ARG_MATRICULA = "matricula"
private const val ARG_NOMBRE = "nombre"
private const val ARG_DOMICILIO = "domicilio"
private const val ARG_ESPECIALIDAD = "especialidad"
private const val ARG_FOTO = "foto"

class DbFragment : Fragment() {
    private var matricula: String? = null
    private var nombre: String? = null
    private var domicilio: String? = null
    private var foto: String? = null
    private var especialidad: String? = null

    private lateinit var txtMatricula: EditText
    private lateinit var txtNombre: EditText
    private lateinit var txtDomicilio: EditText
    private lateinit var txtEspecialidad: EditText
    private lateinit var lblUrlFoto: TextView
    private lateinit var imgAlumno : ImageView
    private lateinit var btnGuardar: Button
    private lateinit var btnBuscar: Button
    private lateinit var btnBorrar: Button
    private lateinit var db: dbAlumnos
    private lateinit var especialidadesValidas: Array<String>

    private val REQUEST_CODE_PICK_IMAGE = 100
    private val REQUEST_CODE_STORAGE_PERMISSION = 200
    private var imageUri: Uri? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            matricula = it.getString(ARG_MATRICULA)
            nombre = it.getString(ARG_NOMBRE)
            domicilio = it.getString(ARG_DOMICILIO)
            especialidad = it.getString(ARG_ESPECIALIDAD)
            foto = it.getString(ARG_FOTO)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_db, container, false)

        // Configurar el texto del encabezado
        val headerTitle: TextView = view.findViewById(R.id.header_title)
        headerTitle.text = getString(R.string.sfrmDb)

        txtMatricula = view.findViewById(R.id.txtMatricula)
        txtNombre = view.findViewById(R.id.txtNombre)
        txtDomicilio = view.findViewById(R.id.txtDomicilio)
        txtEspecialidad = view.findViewById(R.id.txtEspecialidad)
        btnGuardar = view.findViewById(R.id.btnGuardar)
        btnBuscar = view.findViewById(R.id.btnBuscar)
        btnBorrar = view.findViewById(R.id.btnBorrar)
        imgAlumno = view.findViewById(R.id.imgAlumno)
        lblUrlFoto = view.findViewById(R.id.txtFoto)

        especialidadesValidas = resources.getStringArray(R.array.especialidades_validas)

        matricula?.let {
            txtMatricula.setText(it)
            txtMatricula.isEnabled = false // Deshabilitar la matricula cuando reciba argumentos
        }
        nombre?.let { txtNombre.setText(it) }
        domicilio?.let { txtDomicilio.setText(it) }
        especialidad?.let { txtEspecialidad.setText(it) }
        foto?.let { uploadImg(it) }
        foto?.let { lblUrlFoto.text = it }

        btnGuardar.setOnClickListener {
            guardarActualizarAlumno()
        }

        btnBuscar.setOnClickListener {
            buscarAlumno()
        }

        btnBorrar.setOnClickListener {
            confirmdeleteAlumno()
        }

        // Evento para cargar imagenes
        imgAlumno.setOnClickListener {
            solicitarAccesoGaleria()
        }

        txtMatricula.addTextChangedListener(object : TextWatcher{
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) { }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                enableDeleteButton(!s.isNullOrEmpty())
            }

            override fun afterTextChanged(s: Editable?) { }
        })

        lblUrlFoto.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) { }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) { }

            override fun afterTextChanged(s: Editable?) {
                val url = s?.toString() ?: ""
                if (url.isNotEmpty()) {
                    uploadImg(url)
                }
            }
        })
        // Deshabilitamos el botón de borrar cuando la matrícula está vacía
        enableDeleteButton(!txtMatricula.text.isNullOrEmpty())
        return view
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK && requestCode == REQUEST_CODE_PICK_IMAGE) {
            data?.data?.let { uri ->
                imageUri = uri
                imgAlumno.setImageURI(uri) // Mostrar la imagen cargada
                lblUrlFoto.text = uri.toString() // Actualizamos la etiqueta URL de la imagen
            }
        }
    }

    private fun guardarActualizarAlumno() {
        val matricula = txtMatricula.text?.toString() ?: ""
        val nombre = txtNombre.text?.toString() ?: ""
        val domicilio = txtDomicilio.text?.toString() ?: ""
        val especialidad = txtEspecialidad.text?.toString() ?: ""

        if (matricula.isEmpty() || nombre.isEmpty() || domicilio.isEmpty() || especialidad.isEmpty()) {
            Toast.makeText(requireContext(), "Faltan por rellenar Campos.", Toast.LENGTH_SHORT).show()
        } else {
            val dbHelper = AlumnosDbHelper(requireContext())

            // Obtener la URL de la foto al guardar un registro
            val imgUrl = if (imageUri != null) {
                imageUri.toString() // Usar la imagen cargada
            } else {
                lblUrlFoto.text?.toString() ?: foto ?: "Pendiente" // Utilizar una imagen predeterminada por si no sube una imagen
            }

            val alumno = Alumno().apply {
                this.matricula = matricula
                this.nombre = nombre
                this.domicilio = domicilio
                this.especialidad = especialidad
                this.foto = imgUrl
            }

            val alumnoExistente = dbHelper.getAlumnoByMatricula(matricula)
            // Verificar que no se repitan las matrículas
            if (alumnoExistente.id != 0) {
                // Modificar
                if (alumnoExistente.nombre != nombre || alumnoExistente.domicilio != domicilio ||
                    alumnoExistente.especialidad != especialidad || alumnoExistente.foto != lblUrlFoto.text.toString()) {
                    val rowsAffected = dbHelper.actualizarAlumno(alumno, alumnoExistente.id)
                    if (rowsAffected > 0) {
                        Toast.makeText(requireContext(), "Registro actualizado exitosamente.", Toast.LENGTH_SHORT).show()
                        val matricula = arguments?.getString(ARG_MATRICULA)
                        if (!matricula.isNullOrEmpty()) {
                            requireActivity().supportFragmentManager.popBackStack()
                        }
                    } else {
                        Toast.makeText(requireContext(), "Hubo un error al editar el alumno.", Toast.LENGTH_SHORT).show()
                    }
                } else {
                    Toast.makeText(requireContext(), "No se realizo ningún cambio al Alumno.", Toast.LENGTH_SHORT).show()
                }
            } else {
                // Agregar
                val id: Long = dbHelper.agregarAlumno(alumno)
                Toast.makeText(requireContext(), "Registro agregado exitosamente con ID: $id", Toast.LENGTH_SHORT).show()
            }
        }
        if (especialidad !in especialidadesValidas) {
            Toast.makeText(requireContext(), "Especialidad no válida, favor de verificar.", Toast.LENGTH_SHORT).show()
        }
        clearFields()
    }

    private fun buscarAlumno() {
        val matricula = txtMatricula.text?.toString() ?: ""
        if (matricula.isEmpty()) {
            Toast.makeText(requireContext(), "Falta por capturar la Matrícula", Toast.LENGTH_SHORT).show()
        } else {
            val dbHelper = AlumnosDbHelper(requireContext())
            val alumno = dbHelper.getAlumnoByMatricula(matricula)
            if (alumno.id != 0) {
                txtNombre.setText(alumno.nombre)
                txtDomicilio.setText(alumno.domicilio)
                txtEspecialidad.setText(alumno.especialidad)
                // Cuando se encuentra al alumno hay que mostrar la imagen si hay una cargada
                if (alumno.foto != "Pendiente") {
                    uploadImg(alumno.foto)
                    lblUrlFoto.text = alumno.foto
                } else {
                    imgAlumno.setImageResource(R.mipmap.default_picture)
                    lblUrlFoto.text = ""
                }

                // Actualizar la imagen para cargar la actual del alumno
                if (alumno.foto != "pendiente") {
                    imageUri = Uri.parse(alumno.foto)
                } else {
                    imageUri = null
                }
            } else {
                Toast.makeText(requireContext(), "¡¡¡No se encontró la matrícula!!!", Toast.LENGTH_SHORT).show()
            }
        }
    }

    // Verificación para eliminar correctamente un alumno
    private fun confirmdeleteAlumno() {
        val matricula = txtMatricula.text?.toString() ?: ""
        if (matricula.isEmpty()) {
            Toast.makeText(requireContext(), "Falta capturar la matrícula", Toast.LENGTH_SHORT).show()
        } else {
            val builder = AlertDialog.Builder(requireContext())
            builder.setTitle("Confirmación de eliminación de Alumno")
            builder.setMessage("¿Estás seguro de eliminar este alumno?, este cambio puede ser irreversible")

            // Configurar un botón para la Aceptación de eliminación
            builder.setPositiveButton("Aceptar") { dialog, which ->
                deleteAlumno()
                dialog.dismiss()
            }
            // Configurar un botón para la Cancelación de eliminación
            builder.setNegativeButton("Cancelar") { dialog, which ->
                dialog.dismiss()
            }
            val dialog = builder.create()
            dialog.setOnShowListener {
                val positiveButton = dialog.getButton(AlertDialog.BUTTON_POSITIVE)
                positiveButton.setTextColor(ContextCompat.getColor(requireContext(), R.color.aceptar))
                val negativeButton = dialog.getButton(AlertDialog.BUTTON_NEGATIVE)
                negativeButton.setTextColor(ContextCompat.getColor(requireContext(), R.color.cancelar))
            }
            dialog.show()
        }
    }

    private fun deleteAlumno() {
        val matricula = txtMatricula.text?.toString() ?: ""
        val dbHelper = AlumnosDbHelper(requireContext())
        val alumno = dbHelper.getAlumnoByMatricula(matricula)
        if (alumno.id != 0) {
            val result = dbHelper.borrarAlumno(alumno.id)
            val matricula = arguments?.getString(ARG_MATRICULA)
            if (!matricula.isNullOrEmpty()) {
                requireActivity().supportFragmentManager.popBackStack()
            }
            if (result > 0) {
                Toast.makeText(requireContext(), "Eliminación exitosa.", Toast.LENGTH_SHORT).show()
                clearFields()
            } else {
                Toast.makeText(requireContext(), "Hubo un error al eliminar el Alumno.", Toast.LENGTH_SHORT).show()
            }
        } else {
            Toast.makeText(requireContext(), "¡¡¡Alumno no encontrado!!!", Toast.LENGTH_SHORT).show()
        }
    }

    private fun uploadImg(urlImagen: String) {
        Glide.with(requireContext())
            .load(Uri.parse(urlImagen))
            .placeholder(R.mipmap.default_picture)
            .error(R.mipmap.default_picture)
            .into(imgAlumno)
    }

    private fun solicitarAccesoGaleria(){
        if (ContextCompat.checkSelfPermission(requireContext(), Manifest.permission.READ_EXTERNAL_STORAGE)
            != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(
                requireActivity(),
                arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE),
                REQUEST_CODE_STORAGE_PERMISSION
            )
        } else {
            abrirGaleria()
        }
    }

    private fun abrirGaleria() {
        val intent = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
        startActivityForResult(intent, REQUEST_CODE_PICK_IMAGE)
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == REQUEST_CODE_STORAGE_PERMISSION && grantResults.isNotEmpty() &&
            grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            abrirGaleria()
        } else {
            Toast.makeText(context, "Permiso denegado", Toast.LENGTH_SHORT).show()
        }
    }

    private fun enableDeleteButton(enable: Boolean) {
        btnBorrar.isEnabled = enable
        btnBorrar.isClickable = enable
        if (enable) {
            btnBorrar.alpha = 1f
        } else {
            btnBorrar.alpha = 0.5f
        }
    }

    companion object {
        @JvmStatic
        fun newInstance(matricula: String? = null, nombre: String? = null, domicilio: String? = null,
                        especialidad: String? = null, foto: String? = null) =
            DbFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_MATRICULA, matricula)
                    putString(ARG_NOMBRE, nombre)
                    putString(ARG_DOMICILIO, domicilio)
                    putString(ARG_ESPECIALIDAD, especialidad)
                    putString(ARG_FOTO, foto)
                }
            }
    }

    private fun clearFields() {
        txtMatricula.text.clear()
        txtNombre.text.clear()
        txtDomicilio.text.clear()
        txtEspecialidad.text.clear()
        imgAlumno.setImageResource(R.mipmap.default_picture)
        lblUrlFoto.text = ""
    }
}