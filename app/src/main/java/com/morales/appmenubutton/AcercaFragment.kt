package com.morales.appmenubutton

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuInflater
import android.view.View
import android.view.ViewGroup
import android.widget.SearchView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.morales.appmenubutton.R
import com.morales.appmenubutton.DataBase.Alumno
import com.morales.appmenubutton.DataBase.AlumnosDbHelper
import com.google.android.material.floatingactionbutton.FloatingActionButton

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [AcercaFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class AcercaFragment : Fragment() {
    private lateinit var rcvLista : RecyclerView
    private lateinit var adaptador: MiAdaptador
    private lateinit var addAlumno : FloatingActionButton
    private lateinit var searchView: SearchView
    private lateinit var toolbar: Toolbar
    private var listAlumnos : List<Alumno> = emptyList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val View = inflater.inflate(R.layout.fragment_acerca, container, false)

        // Configurar el texto del encabezado
        val headerTitle: TextView = View.findViewById(R.id.header_title)
        headerTitle.text = getString(R.string.sfrmAc)

        // Inicializar variables
        addAlumno = View.findViewById(R.id.addAlumno)
        rcvLista = View.findViewById(R.id.recId)
        toolbar = View.findViewById(R.id.toolbar)

        (activity as? AppCompatActivity)?.setSupportActionBar(toolbar)

        rcvLista.layoutManager = LinearLayoutManager(requireContext())

        var dbHelper = AlumnosDbHelper(requireContext())
        listAlumnos = dbHelper.getAlumnos()

        adaptador = MiAdaptador(listAlumnos, requireContext()) { alumno ->
            val dbFragment = DbFragment.newInstance(alumno.matricula, alumno.nombre, alumno.domicilio, alumno.especialidad, alumno.foto)
            requireActivity().supportFragmentManager.beginTransaction()
                .replace(R.id.frmContenedor, dbFragment)
                .addToBackStack(null)
                .commit()
        }
        rcvLista.adapter = adaptador

        addAlumno.setOnClickListener{
            val dbFragment = DbFragment()

            requireActivity().supportFragmentManager.beginTransaction()
                .replace(R.id.frmContenedor, dbFragment)
                .commit()
        }
        return View
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.scearchview,menu)
        // Agregar el búscador de lista SearchView
        val searchItem = menu.findItem(R.id.srcList)
        searchView = searchItem?.actionView as SearchView

        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener{
            override fun onQueryTextSubmit(query: String?): Boolean {
                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                if (newText != null) {
                    filterList(newText)
                }
                return true
            }
        })
        super.onCreateOptionsMenu(menu, inflater)
    }

    private fun filterList(query: String) {
        // Limpiar la consulta de espacios en blanco innecesarios
        val trimmedQuery = query.trim()

        // Dividir la consulta en palabras clave
        val keywords = trimmedQuery.split("\\s+".toRegex())

        // Filtrar la lista utilizando las palabras clave
        val filteredList = listAlumnos.filter { alumno ->
            keywords.any { keyword ->
                alumno.matricula.contains(keyword, ignoreCase = true) ||
                        alumno.nombre.contains(keyword, ignoreCase = true) ||
                        alumno.especialidad.contains(keyword, ignoreCase = true)
            }
        }
        // Actualizar la lista filtrada
        adaptador.actualizarLista(filteredList)
    }
}