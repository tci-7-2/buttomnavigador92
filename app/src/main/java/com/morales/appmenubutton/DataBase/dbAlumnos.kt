package com.morales.appmenubutton.DataBase

import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase

class dbAlumnos(private val context: Context) {
    private val dbHelper : AlumnosDbHelper = AlumnosDbHelper(context)
    private lateinit var db : SQLiteDatabase

    private val leerRegistro = arrayOf(
        DefinirDB.Alumnos.ID,
        DefinirDB.Alumnos.MATRICULA,
        DefinirDB.Alumnos.NOMBRE,
        DefinirDB.Alumnos.DOMICILIO,
        DefinirDB.Alumnos.ESPECIALIDAD,
        DefinirDB.Alumnos.FOTO
    )

    // Abrir la base de datos
    fun openDataBase(){
        db = dbHelper.writableDatabase
    }
    fun close() {
        dbHelper.close()
    }

    // Insertar alumnos
    fun insertarAlumno(alumno: Alumno): Long {
        val value = ContentValues().apply {
            put(DefinirDB.Alumnos.MATRICULA, alumno.matricula)
            put(DefinirDB.Alumnos.NOMBRE, alumno.nombre)
            put(DefinirDB.Alumnos.DOMICILIO, alumno.domicilio)
            put(DefinirDB.Alumnos.ESPECIALIDAD, alumno.especialidad)
            put(DefinirDB.Alumnos.FOTO, alumno.foto)
        }
        // Manejo de error para verificar en la clave única
        return try {
            db.insert(DefinirDB.Alumnos.TABLA, null, value)
        } catch (e: Exception) {
            -1
        }
    }

    // Actualizar Alumnos
    fun actualizarAlumno(alumno: Alumno, id: Int): Int {
        val values = ContentValues().apply {
            put(DefinirDB.Alumnos.MATRICULA, alumno.matricula)
            put(DefinirDB.Alumnos.NOMBRE, alumno.nombre)
            put(DefinirDB.Alumnos.DOMICILIO, alumno.domicilio)
            put(DefinirDB.Alumnos.ESPECIALIDAD, alumno.especialidad)
            put(DefinirDB.Alumnos.FOTO, alumno.foto)
        }
        return db.update(DefinirDB.Alumnos.TABLA, values, "${DefinirDB.Alumnos.ID} = ?", arrayOf(id.toString()))
    }

    // Borrar Alumnos
    fun borrarAlumno(id: Int): Int {
        return db.delete(DefinirDB.Alumnos.TABLA, "${DefinirDB.Alumnos.ID} = ?", arrayOf(id.toString()))
    }

    // Mostrar Alumnos
    fun mostrarAlumnos(cursor: Cursor): Alumno {
        return Alumno().apply {
            id = cursor.getInt(0)
            matricula = cursor.getString(1)
            nombre = cursor.getString(2)
            domicilio = cursor.getString(3)
            especialidad = cursor.getString(4)
            foto = cursor.getString(5)
        }
    }

    // Buscar Alumnos por ID
    fun getAlumno(id: String): Alumno {
        val cursor = db.query(
            DefinirDB.Alumnos.TABLA, leerRegistro,
            "${DefinirDB.Alumnos.ID} = ?", arrayOf(id.toString()), null,
            null, null
        )
        cursor.moveToFirst()
        val alumno = mostrarAlumnos(cursor)
        cursor.close()
        return alumno
    }

    fun getAlumnoByMatricula(matricula: String): Alumno? {
        val cursor = db.query(
            DefinirDB.Alumnos.TABLA, leerRegistro,
            "${DefinirDB.Alumnos.MATRICULA} = ?", arrayOf(matricula), null,
            null, null
        )
        return if (cursor.moveToFirst()) {
            val alumno = mostrarAlumnos(cursor)
            cursor.close()
            alumno
        } else {
            cursor.close()
            null
        }
    }
    // Obtener todos los registros
    fun leerTodos(): ArrayList<Alumno> {
        val cursor = db.query(DefinirDB.Alumnos.TABLA, leerRegistro, null, null, null, null, null)
        val listaAlumno = ArrayList<Alumno>()
        cursor.moveToFirst()

        while (!cursor.isAfterLast) {
            val alumno = mostrarAlumnos(cursor)
            listaAlumno.add(alumno)
            cursor.moveToNext()
        }

        cursor.close()
        return listaAlumno
    }
}
