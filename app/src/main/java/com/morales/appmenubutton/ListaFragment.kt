package com.morales.appmenubutton

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.ImageView
import android.widget.ListView
import android.widget.SearchView
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import com.morales.appmenubutton.R

private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

class ListaFragment : Fragment() {
    private lateinit var listView: ListView
    private lateinit var originalList: ArrayList<String>
    private lateinit var filteredList: ArrayList<String>
    private lateinit var adapter: ArrayAdapter<String>
    private lateinit var searchView: SearchView
    private lateinit var imgSearchIcon: ImageView
    private lateinit var headerTitle: TextView

    @SuppressLint("MissingInflatedId")
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_lista, container, false)

        // Configurar el texto del encabezado
        val header: View = view.findViewById(R.id.header)
        headerTitle = header.findViewById(R.id.header_title)
        headerTitle.text = getString(R.string.sfrmLista)

        listView = view.findViewById(R.id.lstAlumnos)
        searchView = view.findViewById(R.id.srcLista)
        imgSearchIcon = view.findViewById(R.id.imgSeachIcon)

        val items = resources.getStringArray(R.array.alumnos)
        originalList = ArrayList(items.toList())
        filteredList = ArrayList(originalList)

        adapter = ArrayAdapter(requireContext(), android.R.layout.simple_list_item_1, filteredList)
        listView.adapter = adapter

        listView.setOnItemClickListener { parent, view, position, id ->
            val alumno: String = parent.getItemAtPosition(position).toString()
            val builder = AlertDialog.Builder(requireContext())
            builder.setTitle("Lista de Alumnos")
            builder.setMessage("$position: $alumno")
            builder.setPositiveButton("OK") { dialog, which -> }
            builder.show()
        }

        imgSearchIcon.setOnClickListener {
            headerTitle.visibility = View.INVISIBLE
            imgSearchIcon.visibility = View.INVISIBLE
            searchView.visibility = View.VISIBLE
            searchView.isIconified = false
        }

        searchView.setOnCloseListener {
            searchView.visibility = View.GONE
            headerTitle.visibility = View.VISIBLE
            imgSearchIcon.visibility = View.VISIBLE
            resetList()
            false
        }

        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                filterList(query)
                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                filterList(newText)
                return false
            }
        })

        return view
    }

    private fun filterList(query: String?) {
        if (query.isNullOrEmpty()) {
            filteredList.clear()
            filteredList.addAll(originalList)
        } else {
            val queryWords = query.lowercase().split(" ")
            val newFilteredList = originalList.filter { item ->
                val itemLower = item.lowercase()
                queryWords.all { word -> itemLower.contains(word) }
            }
            filteredList.clear()
            filteredList.addAll(newFilteredList)
        }
        adapter.notifyDataSetChanged()
        listView.visibility = if (filteredList.isEmpty()) View.GONE else View.VISIBLE
    }

    private fun resetList() {
        filteredList.clear()
        filteredList.addAll(originalList)
        adapter.notifyDataSetChanged()
        listView.visibility = View.VISIBLE
    }
}