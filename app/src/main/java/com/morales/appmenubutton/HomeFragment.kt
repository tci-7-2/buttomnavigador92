package com.morales.appmenubutton

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.Fragment
import com.morales.appmenubutton.R
class HomeFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_home, container, false)

        // Configurar el texto del encabezado
        val headerTitle: TextView = view.findViewById(R.id.header_title)
        headerTitle.text = getString(R.string.sfrmHome)

        // Configurar los iconos para abrir las URL correspondientes
        val facebookIcon: ImageView = view.findViewById(R.id.facebook_icon)
        val gmailIcon: ImageView = view.findViewById(R.id.gmail_icon)
        val linkedinIcon: ImageView = view.findViewById(R.id.linkedin_icon)

        // Set OnClickListener for each icon
        facebookIcon.setOnClickListener {
            openUrl("https://www.facebook.com/jsesteban.moralesniebla")
        }

        gmailIcon.setOnClickListener {
            sendEmail("jseste51@gmail.com")
        }

        linkedinIcon.setOnClickListener {
            openUrl("https://www.linkedin.com/in/jesus-esteban-morales-niebla-0675b2309/")
        }

        return view
    }

    private fun openUrl(url: String) {
        val intent = Intent(Intent.ACTION_VIEW, Uri.parse(url))
        startActivity(intent)
    }

    private fun sendEmail(email: String) {
        val intent = Intent(Intent.ACTION_SENDTO).apply {
            data = Uri.parse("mailto:$email")
        }
        startActivity(intent)
    }
}
